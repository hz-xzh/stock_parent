package com.itheima.stock.controller;

import com.itheima.stock.pojo.entity.SysUser;
import com.itheima.stock.service.UserService;
import com.itheima.stock.vo.req.LoginReqVo;
import com.itheima.stock.vo.resp.LoginRespVo;
import com.itheima.stock.vo.resp.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

//@RestController = @Controller + @ResponseBody
//@Controller: 创建当前类对象,并存放到IOC容器中
//@ResponseBody: 将方法的返回值对象,转成json并写回给浏览器
@RestController
// @RequestMapping: 是SpringMVC提供的注解,用于建立请求路径与方法的对应关系
@RequestMapping("/api")
@Api(tags = "用户模块相关接口定义")
public class UserController {

    @Autowired
    private UserService userService;
    // 生成验证码,将验证码存放到Redis中
    @GetMapping("/captcha")
    public R<Map> getCaptchaCode(){
        return userService.getCaptchaCode();
    }


    /**
     * @RequestBody: 解析请求携带的json字符串,并将解析到的内容,封装到对应的实体中
     *  注意: 属性名必须保持一致
     * @param loginReqVo
     */
    @PostMapping("/login")
    public R<LoginRespVo> login(@RequestBody LoginReqVo loginReqVo){
        System.out.println(loginReqVo);
        return userService.login(loginReqVo);
    }

    /**
     * 根据用户名查询用户信息
     * @param userName
     * @PathVariable: 作用,解析请求路径中对应的参数
     */
    @GetMapping("/{userName}")
    @ApiOperation(value = "根据用户名查询用户信息",notes = "用户信息查询",response = SysUser.class)
    @ApiImplicitParam(paramType = "path",name = "userName",value = "用户名",required = true)
    public SysUser getUserByUserName(@PathVariable("userName") String userName){
        return userService.getUserByUserName(userName);
    }

}
