package com.itheima.stock;

import com.itheima.stock.config.SwaggerConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * SpringBoot项目的引导类/入口类
 */
@SpringBootApplication
// @MapperScan: 扫描mapper包,创建接口的代理类对象,并将代理类对象存放到IOC容器中.
@MapperScan("com.itheima.stock.mapper")
@Import(value = {SwaggerConfiguration.class})//导入swagger配置
public class StockApp {
    public static void main(String[] args) {
        SpringApplication.run(StockApp.class,args);
    }
}
