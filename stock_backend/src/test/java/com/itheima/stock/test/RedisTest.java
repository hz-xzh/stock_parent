package com.itheima.stock.test;

import com.itheima.stock.utils.IdWorker;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.UUID;

@SpringBootTest
public class RedisTest {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private IdWorker idWorker;


    @Test
    public void test01(){
        // 往redis中存放数据信息
        // 操作String类型的数据
        ValueOperations ops = redisTemplate.opsForValue();
        //ops.set("ckcode","5566");
        long id = idWorker.nextId();
        ops.set(id+"","5566");
        System.out.println("设置成功...");
    }

    @Test
    public void test02(){
        // 从redis中获取数据信息
        // 操作String类型的数据
        ValueOperations ops = redisTemplate.opsForValue();
        Object ckcode = ops.get("ckcode");
        System.out.println(ckcode);
        System.out.println("获取成功...");
    }

    // 生成唯一标识
    @Test
    public void test03(){
        // UUID生成唯一标记
//        for (int i = 0; i < 100; i++) {
//            String str = UUID.randomUUID().toString();
//            System.out.println(str);
//        }
        // 雪花算法生成唯一比较
        // 雪花算法可以生成64位连续的数值
        for (int i = 0; i < 10000; i++) {
            long id = idWorker.nextId();
            System.out.println(id);
        }
    }

    @Test
    public void test04(){
        for (int i = 0; i < 10; i++) {
            //String num = RandomStringUtils.randomNumeric(4);
            //System.out.println(num);
            String str = RandomStringUtils.randomAlphanumeric(4);
            System.out.println(str);
        }
    }
}
