package com.itheima.stock.test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootTest
public class BCryptTest {

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * BCrypt: 加密算法不可逆
     * todo:
     *  测试密码加密
     *  123456 --> 给明文中加入随机的盐值
     *  $2a$10$Wg8RGQS0rBLpWflTGWt/yeZCWN9kSiBnWcPkHpLRYavsA750q3fAK
     *  $2a$10$OFZKtTGxSLqcQoo7moGIdur8k9ZC.pMtP94b6h/EiRpbr/VOcTGWa
     *  $2a$10$5sgC8L2tfmsO6Vy/rNq0w.oiCZS5ffsyLO.qxUCsqED4QYq18FdAS
     *  $2a$10$pLb9lAmDKafVH/pRKfxNKe3V7DSZaitg5KUm62HjyGPK0ttmtGG/K
     *  $2a$10$cO6lE6WcQ3IM7NifGJ7E7uJ9Ntk4U978STNH8k1YP6LNm/zN9XAP.
     *  $2a$10$PP1C9.k98.Gf86dcoYt8.uJIYmHT0Xc8EBvKZWtyGvDRaWzEecqZi
     *  $2a$10$0oh.A9VIpKtC5Vdu8sn97edQ8aYhll85PtaWeG/bf.D6YYQz4HpHu
     *  $2a$10$cRsngTXeVbWli64jc1w5J.PNq04aKAq01HR7pnZL3G3HeweCalrfi
     *  $2a$10$LWRp6EcKMpau76VfE1xQROoNXgGhsG4OIT9AOrdGTXlGjjM7Py7a.
     *  $2a$10$5VIGrEuA40q5JW5sdHMzj.66AfjeZ6scYMM5pdxrR8eSMEPs5ElFG
     */
    @Test
    public void testEncoder(){
        // 将同一个明文字符串加密多次
        for (int i = 0; i < 10; i++) {
            String encode = passwordEncoder.encode("123456");
            System.out.println(encode);
        }
    }

    /**
     * todo:
     *  密码认证/校验:
     *      从密文中分析出对应的盐值,将盐值加入到明文中,再使用相同的加密算法进行加密,对比加密后的密文.
     */
    @Test
    public void decode(){
        String str = "123456";
        String encode = "$2a$10$0oh.A9VIpKtC5Vdu8sn97edQ8aYhll85PtaWeG/bf.D6YYQz4HpHu";
        encode = "$2a$10$5VIGrEuA40q5JW5sdHMzj.66AfjeZ6scYMM5pdxrR8eSMEPs5ElFG";
        // 参数1: 明文
        // 参数2: 密文
        boolean flag = passwordEncoder.matches(str,encode);
        System.out.println(flag);
    }
}
