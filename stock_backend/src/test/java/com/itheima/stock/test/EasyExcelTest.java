package com.itheima.stock.test;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.itheima.stock.pojo.User;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EasyExcelTest {
    /**
     * todo:
     *  mock数据:准备一些数据
     * @return
     */
    public List<User> init(){
        //组装数据
        ArrayList<User> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setAddress("上海"+i);
            user.setUserName("张三"+i);
            user.setBirthday(new Date());
            user.setAge(10+i);
            users.add(user);
        }
        return users;
    }

    @Test
    public void test01(){
        // 获取数据
        List<User> list = init();
        // 将list集合中的数据导出到Excel中
        // 参数1: 工作簿名称
        // 参数2: 一行存放一个User对象
        //EasyExcel.write("C:\\Users\\85190\\Desktop\\excel\\用户.xls",User.class).sheet("用户信息").doWrite(list);
        //EasyExcel.write("C:\\Users\\85190\\Desktop\\excel\\用户2.xls",User.class).sheet("用户信息").doWrite(list);
        //EasyExcel.write("C:\\Users\\85190\\Desktop\\excel\\用户3.xls",User.class).sheet("用户信息").doWrite(list);
        //EasyExcel.write("C:\\Users\\85190\\Desktop\\excel\\用户4.xls",User.class).sheet("用户信息").doWrite(list);
        //EasyExcel.write("C:\\Users\\85190\\Desktop\\excel\\用户5.xls",User.class).sheet("用户信息").doWrite(list);
        //EasyExcel.write("C:\\Users\\85190\\Desktop\\excel\\用户6.xls",User.class).sheet("用户信息").doWrite(list);
        EasyExcel.write("C:\\Users\\85190\\Desktop\\excel\\用户7.xls",User.class).sheet("用户信息").doWrite(list);
        System.out.println("导出成功...");
    }

    @Test
    public void test02(){
        //定义一个list集合,用于收集从Excel中读取的数据信息
        ArrayList<User> users = new ArrayList<>();
        //从Excel中读取数据
        // 参数1: 被读取的Excel文件所在的位置
        // 参数2: 每一行读取后存放到哪个对象中
        // 参数3: 事件监听(每读取一行,触发一次事件)
        EasyExcel.read("C:\\Users\\85190\\Desktop\\excel\\用户7.xls",User.class,new AnalysisEventListener<User>(){
            // 每读取一行调用一次这个方法
            @Override
            public void invoke(User user, AnalysisContext analysisContext) {
                System.out.println(user);
                users.add(user);
            }
            // 读取完毕后,调用此方法
            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                System.out.println("读取完毕...");
            }
        }).sheet("用户信息").doRead();
        System.out.println("------------------------");
        System.out.println(users);
    }

}
