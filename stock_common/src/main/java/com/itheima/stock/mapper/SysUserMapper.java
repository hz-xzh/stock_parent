package com.itheima.stock.mapper;

import com.itheima.stock.pojo.entity.SysUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
* @author 85190
* @description 针对表【sys_user(用户表)】的数据库操作Mapper
* @createDate 2023-01-06 11:41:14
* @Entity com.itheima.stock.pojo.entity.SysUser
*/
public interface SysUserMapper {

    // 根据用户名查询用户详情
    SysUser findByUserName(@Param("name") String userName);

    int deleteByPrimaryKey(Long id);
    // 添加所有字段
    int insert(SysUser record);
    // 有选择性的添加: 当添加的字段不为空时,添加
    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

}
