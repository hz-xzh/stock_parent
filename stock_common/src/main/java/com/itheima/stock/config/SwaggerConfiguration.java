package com.itheima.stock.config;
import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
//开启swagger支持
@EnableSwagger2
// 开启Knife4j支持
@EnableKnife4j
// 开启Knife4j校验
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfiguration {
   // 将方法的返回值存放到IOC容器中
   @Bean
   public Docket buildDocket() {
      //构建在线API概要对象
      return new Docket(DocumentationType.SWAGGER_2)
              // api的基本信息
              .apiInfo(buildApiInfo())
              .select()
              // 要扫描的API(Controller)基础包
              .apis(RequestHandlerSelectors.basePackage("com.itheima.stock.controller"))
              .paths(PathSelectors.any())
              .build();
   }
   private ApiInfo buildApiInfo() {
      //网站联系方式
      Contact contact = new Contact("黑马程序员","https://www.itheima.com/","itcast@163.com");
      return new ApiInfoBuilder()
              .title("今日指数-在线接口API文档")
              .description("这是一个方便前后端开发人员快速了解开发接口需求的在线接口API文档")
              .contact(contact)
              .version("1.0.0").build();
   }
}